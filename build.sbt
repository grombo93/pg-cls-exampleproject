import sbt.Keys._
import sbt.Resolver

lazy val commonSettings = Seq(
  version := "1.0",
  organization := "de.tu_dortmund.cs.ls14",

  scalaVersion := "2.12.4",

  resolvers ++= Seq(
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots"),
    Resolver.typesafeRepo("releases")
  ),

  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-language:implicitConversions"
  )
)

lazy val core = (Project(id = "cls-spinal-surgery", base = file(".")))
  .settings(commonSettings: _*)
  .settings(
    moduleName := "cls-spinal-surgery",
    exportJars := true,
    libraryDependencies ++= Seq(
      "org.combinators" % "cls-scala_2.12" % "2.0.0+9-e4d7e827",
      "org.scala-lang" % "scala-compiler" % scalaVersion.value,
      "log4j" % "log4j" % "1.2.17"
      )
  )




