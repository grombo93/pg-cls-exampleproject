package de.tu_dortmund.cs.ls14.spinal_surgery

import org.combinators.cls.types.{Kinding, Taxonomy}
import org.combinators.cls.types.syntax._

trait SurgeryTaxKindingRepository extends Repository {
  lazy val taxList = Taxonomy("surgery").addSubtype("sp_surgery") ::
    Taxonomy("sp_surgery")
      .addSubtype("sp_open_discectomy")
      .addSubtype("sp_minimal_invasive") ::
    Taxonomy("sp_minimal_invasive")
      .addSubtype("sp_microendoscopic_discectomy")
      .addSubtype("sp_laser_discectomy")
      .addSubtype("sp_chemonucleolysis")
      .addSubtype("sp_percutaneous_discectomy") ::
    Taxonomy("anesthesia")
      .addSubtype("general_anesthesia")
      .addSubtype("local_anesthesia") ::
    Nil

  lazy val semanticTaxonomy = taxList.foldLeft(Taxonomy.empty)((a,b) => a.merge(b))

  lazy val kindingList =
    Kinding(alpha)
      .addOption('sp_laser_discectomy)
      .addOption('sp_open_discectomy)
      .addOption('sp_microendoscopic_discectomy)
      .addOption('sp_chemonucleolysis)
      .addOption('sp_percutaneous_discectomy)

  val kinding : Kinding = kindingList
}