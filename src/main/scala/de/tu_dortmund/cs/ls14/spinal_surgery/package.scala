package de.tu_dortmund.cs.ls14

import org.combinators.cls.types.{Constructor, Variable}

package object spinal_surgery {

  lazy val sp_surgery = Constructor("sp_surgery")
  lazy val sp_open_discectomy = Constructor("sp_open_discectomy")
  lazy val sp_minimal_invasive = Constructor("sp_minimal_invasive")
  lazy val sp_microendoscopic_discectomy = Constructor("sp_microendoscopic_discectomy")
  lazy val sp_laser_discectomy = Constructor("sp_laser_discectomy")
  lazy val sp_chemonucleolysis = Constructor("sp_chemonucleolysis")
  lazy val sp_percutaneous_discectomy = Constructor("sp_percutaneous_discectomy")
  lazy val surgery = Constructor("surgery")
  lazy val nodiabetes_proc = Constructor("nodiabetes_proc")
  lazy val diabetes_proc = Constructor("diabetes_proc")

  lazy val general_anesthesia = Constructor("general_anesthesia")
  lazy val local_anesthesia = Constructor("local_anesthesia")
  lazy val anesthesia = Constructor("anesthesia")

  lazy val task = Constructor("task")

  lazy val alpha = Variable("alpha")

}
