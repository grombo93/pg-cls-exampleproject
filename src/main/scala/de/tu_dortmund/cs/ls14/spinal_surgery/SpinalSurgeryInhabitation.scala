package de.tu_dortmund.cs.ls14.spinal_surgery

import org.combinators.cls.interpreter.{InhabitationResult, ReflectedRepository}
import org.combinators.cls.inhabitation.Tree
import org.combinators.cls.types.{Type, _}
import org.combinators.cls.types.syntax._

object SpinalSurgeryInhabitation extends App {

  lazy val Gamma = new SurgeryTaxKindingRepository {}

  lazy val reflectedGamma = ReflectedRepository(Gamma,Gamma.semanticTaxonomy,Gamma.kinding)

  val inhabitRes = reflectedGamma.inhabit[scala.xml.Elem]('sp_microendoscopic_discectomy :&: 'diabetes_proc)

  if(!inhabitRes.isEmpty && !inhabitRes.isInfinite){
    reflectedGamma.combinators.foreach(printCombinator)
    println()
    println(prettyPrintLatex(0,inhabitRes.terms.index(0),false))
  } else {
    println("Inhabitation failed")
  }

  def printCombinator : ((String,Type)) => Unit = {
    case (str,typ) => {
      print(combinatorName(str) + " : ")
      print(nativeType(typ.toString().substring(0,typ.toString().indexOf('&')).replaceAll("scala.xml.Elem","XmlElem").
        replaceAll("->","\\$\\\\rightarrow\\$")) + " ")
      print(typ.toString().substring(typ.toString().indexOf('&'),typ.toString().indexOf('&') + 1).replaceAll("&","\\$\\\\cap\\$"))
      print(typ.toString().substring(typ.toString().indexOf('&') + 1).replaceAll("&","\\$\\\\cap\\$").replaceAll("->","\\$\\\\rightarrow\\$").
        replaceAll("alpha","\\$\\\\alpha\\$").replaceAll("_","\\\\_"))
      println()
    }
  }

  def printTabs(n:Int):String = n match {
    case 0 => ""
    case n => "\t" + printTabs(n-1)
  }

  def printTabsLatex(n:Int):String = n match {
    case 0 => " "
    case n => "\\hspace{1cm}" + printTabsLatex(n-1)
  }


  lazy val printListBegin = (b:Boolean,n:Int) => b match  {
    case true => printTabs(n+1) + "List" + "(" + "\n"
    case false => ""
  }

  lazy val printListEnd = (b:Boolean,n:Int) => b match  {
    case true =>  printTabs(n+1) +")" + "\n"
    case false => ""
  }

  lazy val printListBeginLatex = (b:Boolean,n:Int) => b match {
    case true => printTabsLatex(n+1) + list + "(" + "\n\n"
    case false => "";
  }

  lazy val printListEndLatex = (b:Boolean,n:Int) => b match {
    case true => printTabsLatex(n+1) + ")" + "\n\n"
    case false => ""
  }

  lazy val combinatorName = (str:String) =>  {
    "\\combinatorName{" + str + "}"
  }

  lazy val nativeType = (str:String) => {
    "\\nativeType{" + str + "}"
  }

  lazy val treeName = (str:String) => {
    "\\treeName{" + str + "}"
  }

  lazy val tree = "\\tree"

  lazy val list = "\\listMak"


  def prettyPrint(n:Int,t:Tree,printList:Boolean):String = {
      printTabs(n) + "Tree" + "(" + "\n" + printTabs(n) + t.name +  "\n" +
      printListBegin(printList,n) +
      t.arguments.map(( a:Tree )  => {prettyPrint(n+1, a,printList)}).foldLeft(""){(a:String,b:String)=>{a + b +"\n"}} +
      printListEnd(printList,n) + printTabs(n) + ")"
  }

  def prettyPrintLatex(n:Int,t:Tree,printList:Boolean):String = {
      printTabsLatex(n) + tree + "(" + "\n\n" + printTabsLatex(n) + treeName(t.name) + "\n\n" +
      printListBeginLatex(printList,n) +
      t.arguments.map(( a:Tree )  => {prettyPrintLatex(n+1, a,printList)}).foldLeft(""){(a:String,b:String)=>{a + b +"\n\n"}} +
      printListEndLatex(printList,n) + printTabsLatex(n) + ")"
  }


}