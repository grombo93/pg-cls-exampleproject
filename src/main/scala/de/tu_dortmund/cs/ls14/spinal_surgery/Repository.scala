package de.tu_dortmund.cs.ls14.spinal_surgery

import org.combinators.cls.interpreter.combinator
import org.combinators.cls.types.syntax._
import de.tu_dortmund.cs.ls14.spinal_surgery._
import scala.xml._

import org.combinators.cls.interpreter.combinator

trait AnesthesiaSemanticTypeGeneral{
  def apply : Elem
  val semanticType = general_anesthesia
}

trait AnesthesiaSemanticTypeLocal {
  def apply : Elem
  val semanticType = local_anesthesia
}

trait SurgerySemanticTypeChemo {
  def apply : Elem
  val semanticType = sp_chemonucleolysis :&: task
}

trait SurgerySemanticTypeMicro {
  def apply : Elem
  val semanticType = sp_microendoscopic_discectomy :&: task
}

trait SurgerySemanticTypeLaser{
  def apply : Elem
  val semanticType = sp_laser_discectomy :&: task
}

trait SurgerySemanticTypePercu  {
  def apply : Elem
  val semanticType = sp_percutaneous_discectomy :&: task
}

trait SurgerySemanticTypeOpen  {
  def apply : Elem
  val semanticType = sp_open_discectomy :&: task
}

trait SurgeryDiabetesSemanticType {
  def apply (s:Elem) : Elem
  val semanticType = alpha :&: nodiabetes_proc =>: alpha :&: diabetes_proc

}

trait SpinalSurgerySemanticTypeProc {

  def apply (s1:Elem,s2:Elem):Elem
  val semanticType =
    (local_anesthesia  =>: sp_laser_discectomy :&: task =>: sp_laser_discectomy :&: nodiabetes_proc) :&:
      (local_anesthesia =>: sp_chemonucleolysis :&: task =>: sp_chemonucleolysis :&: nodiabetes_proc) :&:
      (local_anesthesia =>: sp_microendoscopic_discectomy :&: task =>:  sp_microendoscopic_discectomy :&: nodiabetes_proc) :&:
      (local_anesthesia =>: sp_percutaneous_discectomy :&: task =>: sp_percutaneous_discectomy :&: nodiabetes_proc) :&:
      (general_anesthesia =>: sp_open_discectomy :&: task =>: sp_open_discectomy :&: nodiabetes_proc)
}

trait Repository {

  /*
    Anesthesia Tasks
   */
  @combinator object AnesthesiaTaskGeneral
    extends AnesthesiaSemanticTypeGeneral {
    def apply : Elem = <userTask id="anesthesia_task" name="General Anesthesia"></userTask>

  }

  @combinator object AnesthesiaTaskLocal
    extends AnesthesiaSemanticTypeLocal {
    def apply :Elem = <userTask id="anesthesia_task" name="Local Anesthesia"></userTask>
  }

  /*
    Surgery Tasks
   */

  @combinator object SurgeryTaskOpen
    extends SurgerySemanticTypeOpen {
    def apply:Elem = <userTask id="surgery_task" name="Open Discectomy"></userTask>
  }

  @combinator object SurgeryTaskMicro
    extends SurgerySemanticTypeMicro {
    def apply:Elem= <userTask id="surgery_task" name="Microendoscopic Discectomy"></userTask>
  }

  @combinator object SurgeryTaskChemo
    extends SurgerySemanticTypeChemo {
    def apply:Elem = <userTask id="surgery_task" name="Chemonucleolysis Discectomy"></userTask>
  }

  @combinator object SurgeryTaskLaser
    extends SurgerySemanticTypeLaser {
    def apply:Elem = <userTask id="surgery_task" name="Laser Discectomy"></userTask>
  }

  @combinator object SurgeryTaskPercu
    extends SurgerySemanticTypePercu {
    def apply:Elem = <userTask id="surgery_task" name="Percutaneous Discectomy"></userTask>
  }

  /*
    Non-Trivial Combinators
   */
  @combinator object SurgeryDiabetes
    extends SurgeryDiabetesSemanticType {
    def apply(s:Elem): Elem = {
      surgeryDiabetesHelper(s.child)
    }
  }

  def surgeryDiabetesHelper(x:Seq[Node]) : Elem = {
    <process id="surgery_diabetes_Combinator" name="Surgery Diabetes Combinator" isExecutable="true">
      <startEvent id="startevent_surgery_diabetes" name="Start"></startEvent>
      <endEvent id="endevent_surgery_diabetes" name="End"></endEvent>
      <subProcess id="surgery_sub_process" name="Surgery Sub Process"> {x} </subProcess>
      <userTask id="diabetes_care_task" name="Diabetes Care Task"></userTask>
      <sequenceFlow id="flow_start_surgerySubProc" sourceRef="startevent_surgery_diabetes" targetRef="surgery_sub_process"></sequenceFlow>
      <sequenceFlow id="flow_surgerySubProc_diabetesCareTask" sourceRef="surgery_sub_process" targetRef="diabetes_care_task"></sequenceFlow>
      <sequenceFlow id="flow_diabetesCareTask_End" sourceRef="diabetes_care_task" targetRef="endevent_surgery_diabetes"></sequenceFlow>
    </process>
  }

  @combinator object SpinalSurgeryProc
    extends SpinalSurgerySemanticTypeProc {
    def apply(s1:Elem,s2:Elem):Elem ={

      <process id="spinal_sugery_process_combinator" name="Spinal Surgery Process Combinator" isExecutable="true">
        <startEvent id="startevent_spinal_surgery_Process" name="Start"></startEvent>
        <endEvent id="endevent_spinal_surgery_Process" name="End"></endEvent>
        {s1}
        {s2}
        <sequenceFlow id="flow_start_anesthesiaTask" sourceRef="startevent_spinal_surgery_Process" targetRef="anesthesia_task"></sequenceFlow>
        <sequenceFlow id="flow_anesthesiaTask_surgeryTask" sourceRef="anesthesia_task" targetRef="surgery_task"></sequenceFlow>
        <sequenceFlow id="flow_surgeryTask_end" sourceRef="surgery_task" targetRef="endevent_spinal_surgery_Process"></sequenceFlow>
      </process>
    }
  }
}
